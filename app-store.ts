import Vuex from "vuex";
import getters from "./getters";
import auth from './modules/auth';
import browse from './modules/browse';
import businessBookings from './modules/business-bookings';
import businessCheckins from './modules/business-checkins';
import businessItemCategories from './modules/business-item-categories';
import businessItems from './modules/business-items';
import businessCustomers from './modules/business-customers';
import checkin from './modules/checkin';
import device from './modules/device';
import order from './modules/order';
import userBooker from './modules/user-booker';
import userBookings from './modules/user-bookings';
import webUi from './modules/web-ui';

const store = new Vuex.Store({
  modules: {
    auth: auth,
    browse: browse,
    businessBookings: businessBookings,
    businessCheckins: businessCheckins,
    businessItemCategories: businessItemCategories,
    businessItems: businessItems,
    businessCustomers: businessCustomers,
    userBooker: userBooker,
    checkin: checkin,
    device: device,
    order: order,
    userBookings: userBookings,
    webUi: webUi,
  },
  getters
});
export default store;



