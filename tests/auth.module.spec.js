import { mutations }  from '../modules/auth'

// destructure assign `mutations`
const { auth_request } = mutations

describe('mutations', () => {
  it('INCREMENT', () => {
    // mock state
    const state = { count: 0 }
    console.log(state);
    // apply mutation
    auth_request(state)
    console.log(state);
    // assert result
    // expect(state.count).to.equal(1)
  })
})