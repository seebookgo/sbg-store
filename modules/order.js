import axios from "axios";

const getDefaultState = () => {
  return {
    categories: {
      data: [],
      links: {}
    },
    items: {
      data: [],
      links: {}
    },
    basket: {
      serviceId: null,
      meta: {
        id: null,
        count: 0,
        total: "0.00"
      },
      items: []
    },
    profile_confirmed: false
  };
};

// initial state
const state = getDefaultState();

const actions = {
  profileConfirmed({ commit }, val = true) {
    commit("profileConfirmed", val);
  },
  categories({ commit }, { slug, id }) {
    return new Promise((resolve, reject) => {
      axios
        .get(`api/${slug}/order/${id}`)
        .then(resp => {
          commit("setCategories", resp.data);
          resolve();
        })
        .catch(err => {
          reject(err);
        });
    });
  },
  items({ commit }, { slug, service_id: service_id, id }) {
    return new Promise((resolve, reject) => {
      id = (id == "all") ? "" : id;
      axios
        .get(`api/${slug}/order/${service_id}/items/${id}`)
        .then(resp => {
          commit("setItems", { data: resp.data, serviceId: service_id });
          resolve();
        })
        .catch(err => {
          reject(err);
        });
    });
  },
  getBasket({ commit }, { slug }) {
    return new Promise((resolve, reject) => {
      axios
        .get(`api/${slug}/basket`)
        .then(resp => {
          commit("setBasket", resp.data);
          resolve();
        })
        .catch(err => {
          reject(err);
        });
    });
  },
  addToBasket({ commit }, { slug, id }) {
    return new Promise((resolve, reject) => {
      axios
        .post(`api/${slug}/basket`, { id: id })
        .then(resp => {
          commit("setBasket", resp.data);
          resolve();
        })
        .catch(err => {
          reject(err);
        });
    });
  },
  removeFromBasket({ commit }, { slug, id }) {
    return new Promise((resolve, reject) => {
      axios
        .delete(`api/${slug}/basket/${id}`)
        .then(resp => {
          commit("setBasket", resp.data);
          resolve();
        })
        .catch(err => {
          reject(err);
        });
    });
  },
  reset({ commit }) {
    commit("resetState");
  }
};

const mutations = {
  resetState(state) {
    Object.assign(state, getDefaultState());
  },
  setCategories: (state, data) => {
    state.categories = data;
  },
  setItems: (state, data) => {
    state.basket.serviceId = data.serviceId;
    state.items = data.data;
  },
  setBasket: (state, data) => {
    state.basket.meta = data.meta;
    state.basket.items = data.data;
  },
  profileConfirmed(state, val) {
    state.profile_confirmed = val;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
