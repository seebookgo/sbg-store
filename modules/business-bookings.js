import axios from "axios";

const getDefaultState = () => {
  return {
    bookings: {
      meta: {
        filter: {
          range: {}
        }
      },
      data: [],
      links: {
        first: null
      }
    },
    booking: {
      data: {}
    },
    mode: "current",
    dateRange: {
      startDate: null,
      endDate: null
    }
  };
};

// initial state
const state = getDefaultState();

const actions = {
  list({ commit, state, rootGetters }, page) {
    commit("clear");
    return new Promise(resolve => {
      const params = {};

      if (state.mode == "pending") {
        params["filter[pending]"] = true;
      } else if (state.mode == "past") {
        params["filter[past]"] = true;
      } else if (state.mode == "cancelled") {
        params["filter[cancelled]"] = true;
      } else if (state.mode == "range") {
        params["filter[from]"] = state.dateRange.startDate;
        params["filter[to]"] = state.dateRange.endDate;
      } else {
        params["filter[current]"] = true;
      }
      params["page"] = page;
      if (!rootGetters.business) return false;
      const slug = rootGetters.business.slug;
      axios
        .get(`/api/${slug}/bookings`, {
          params
        })
        .then(response => {
          commit("store", response.data);
          resolve();
        });
    });
  },
  get({ commit, rootGetters }, id) {
    const slug = rootGetters.business.slug;
    return new Promise((resolve, reject) => {
      axios
        .get(`/api/${slug}/bookings/${id}`)
        .then(r => {
          resolve(r.data);
          commit("setBooking", r.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  },
  range({ commit, dispatch }, range) {
    commit("range", range);
    dispatch("list");
  },
  cancel({ dispatch, rootGetters }, booking) {
    const slug = rootGetters.business.slug;
    return new Promise((resolve, reject) => {
      axios
        .delete(`/api/${slug}/bookings/${booking.id}`)
        .then(resp => {
          dispatch("list");
          resolve(resp);
        })
        .catch(err => {
          reject(err);
        });
    });
  },
  confirm({ dispatch }, booking) {
    return dispatch("edit", { status_id: 1, id: booking.id });
  },
  edit({ dispatch, rootGetters }, booking) {
    const slug = rootGetters.business.slug;
    return new Promise((resolve, reject) => {
      if (booking.id) {
        axios
          .put(`/api/${slug}/bookings/${booking.id}`, booking)
          .then(r => {
            dispatch("list");
            resolve(r);
          })
          .catch(err => {
            reject(err);
          });
      } else {
        axios
          .post(`/api/${slug}/bookings`, booking)
          .then(r => {
            dispatch("list");
            resolve(r);
          })
          .catch(err => {
            reject(err);
          });
      }
    });
  }
};

const mutations = {
  resetState(state) {
    Object.assign(state, getDefaultState());
  },
  clear: state => {
    state.bookings = {
      meta: {
        filter: {
          range: {}
        }
      },
      data: [],
      links: {}
    };
  },
  store: (state, data) => {
    state.bookings = data;
  },
  setBooking: (state, data) => {
    state.booking = data;
  },
  mode: (state, data) => {
    state.mode = data;
  },
  range: (state, payload) => {
    state.mode = "range";
    state.dateRange = {
      startDate: payload.startDate,
      endDate: payload.endDate
    };
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
