import axios from "axios";

const getDefaultState = () => {
  return {
    bookings: {
      data: [],
      meta: {},
      links: {}
    },
    booking: {
      data: {
        business: {},
        service: {},
        resource: {},
        status: {}
      }
    },
    mode: "upcoming"
  };
};

// initial state
const state = getDefaultState();

const actions = {
  list({ commit }, page = 1) {
    return new Promise((resolve, reject) => {
      const params = {};
      const mode = state.mode;
      if (mode == "previous") {
        params["filter[previous]"] = true;
      } else if (mode == "upcoming") {
        params["filter[upcoming]"] = true;
      } else if (mode == "cancelled") {
        params["filter[cancelled]"] = true;
      } else if (mode == "orders") {
        params["filter[orders]"] = true;
      } else if (mode == "range") {
        params["filter[from]"] = this.dateRange.startDate;
        params["filter[to]"] = this.dateRange.endDate;
      }
      params["page"] = page;
      
      // if (page == 1) {
      //   commit("resetBookings");
      // }

      axios
        .get(`/api/bookings`, {
          params
        })
        .then(
          r => {
            commit("setBookings", r.data);
            resolve(r.data);
          },
          err => {
            reject(err);
          }
        );
    });
  },
  cancel({ dispatch }, booking) {
    return new Promise((resolve, reject) => {
      axios
        .delete(`/api/bookings/${booking.id}`)
        .then(r => {
          dispatch("list");
          resolve(r.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  },
  get({ commit }, id) {
    return new Promise((resolve, reject) => {
      axios
        .get(`/api/bookings/${id}`)
        .then(r => {
          resolve(r.data);
          commit("setBooking", r.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  },
  resetState({ commit }) {
    commit("resetState");
  }
};

const mutations = {
  resetState(state) {
    Object.assign(state, getDefaultState());
  },
  resetBookings: (state) => {
    state.bookings = getDefaultState().bookings;
  },
  mode: (state, data) => {
    state.mode = data;
  },
  setBookings: (state, bookings) => {
    bookings.data.forEach(booking => {
      state.bookings.data.push(booking);
    });
    state.bookings.links = bookings.links;
    state.bookings.meta = bookings.meta;
  },
  setBooking: (state, booking) => {
    state.booking = booking;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
