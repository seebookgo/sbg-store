import axios from 'axios';

const getDefaultState = () => {
    return {
        customers: {
            meta: {
                filter: {
                    range: {},
                },
            },
            data: [],
            links: {
                first: null,
            },
        },
        customer: null,
        mode: 'current',
        dateRange: {
            startDate: null,
            endDate: null,
        },
        newCustomer: null
    };
};

// initial state
const state = getDefaultState();

const actions = {
    list({ commit, rootState }, page) {
        commit('clear');
        return new Promise((resolve) => {
            const params = {};

            params['page'] = page;
            const slug = rootState.auth.user.current_business.slug;
            axios
                .get(`/api/${slug}/customers`, {
                    params,
                })
                .then((response) => {
                    commit('store', response.data);
                    resolve();
                });
        });
    },
    get({ commit, rootState }, id) {
        const slug = rootState.auth.user.current_business.slug;
        return new Promise((resolve, reject) => {
            axios
                .get(`/api/${slug}/customers/${id}`)
                .then((r) => {
                    resolve(r.data);
                    commit('setCustomer', r.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    },
    range({ commit, dispatch }, range) {
        commit('range', range);
        dispatch('list');
    },
    edit({ dispatch, rootState, commit }, customer) {
        const slug = rootState.auth.user.current_business.slug;
        return new Promise((resolve, reject) => {
            if (customer.id) {
                axios
                    .put(`/api/${slug}/customers/${customer.id}`, customer)
                    .then((r) => {
                        dispatch('list');
                        resolve(r.data);
                    })
                    .catch((err) => {
                        reject(err);
                    });
            } else {
                axios
                    .post(`/api/${slug}/customers`, customer)
                    .then((r) => {
                        commit('setNewCustomer', r.data);
                        dispatch('list');
                        resolve(r.data);
                    })
                    .catch((err) => {
                        reject(err);
                    });
            }
        });
    },
    delete({rootState }, id) {
        const slug = rootState.auth.user.current_business.slug;
        return new Promise((resolve, reject) => {
            axios.delete(`/api/${slug}/customers/${id}`).then(
                () => {
                    resolve();
                },
                () => {
                    reject();
                }
            );
        });
    },
};

const mutations = {
    resetState(state) {
        Object.assign(state, getDefaultState());
    },
    clear: (state) => {
        state.customers = {
            meta: {
                filter: {
                    range: {},
                },
            },
            data: [],
            links: {},
        };
    },
    store: (state, data) => {
        state.customers = data;
    },
    setCustomer: (state, data) => {
        state.customer = data;
    },
    setNewCustomer: (state, data) => {
        state.newCustomer = data;
    },
    mode: (state, data) => {
        state.mode = data;
    },
    range: (state, payload) => {
        state.mode = 'range';
        state.dateRange = {
            startDate: payload.startDate,
            endDate: payload.endDate,
        };
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
};
