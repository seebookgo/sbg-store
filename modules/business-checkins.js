import axios from 'axios';

const getDefaultState = () => {
    return {
        checkins: {
            meta: {
                filter: {
                    range: {}
                }
            },
            data: [],
            links: {
                first: null
            }
        },
        mode: 'current',
        checkedIn: null,
        dateRange: {
            startDate: null,
            endDate: null
        }
    }
};

// initial state
const state = getDefaultState()

const actions = {
    list({ commit, state, rootState }, page) {
        commit('clear');
        const params = {};
        
        if (state.mode == "pending") {
            params["filter[pending]"] = true;
        } else if (state.mode == "cancelled") {
            params["filter[cancelled]"] = true;
        } else if (state.mode == "range") {
            params["filter[from]"] = state.dateRange.startDate;
            params["filter[to]"] = state.dateRange.endDate;
        } else {
            params["filter[current]"] = true;
        }

        params["page"] = page;
        axios
            .get(`/api/${rootState.auth.businessSlug}/check-ins`, {
                params
            })
            .then(response => {
                commit('store', response.data);
            });
    },
    range({ commit, dispatch }, range) {
        commit("range", range);
        dispatch("list")
    }
};

const mutations = {
    resetState(state) {
        Object.assign(state, getDefaultState())
    },
    clear: (state, data) => {
        state.checkins = {
            meta: {
                filter: {
                    range: {}
                }
            },
            data: [],
            links: {}
        };
    },
    store: (state, data) => {
        state.checkins = data;
    },
    mode: (state, data) => {
        state.mode = data;
    },
    range: (state, payload) => {
        state.mode = "range";
        state.dateRange = {
            startDate: payload.startDate,
            endDate: payload.endDate
        };
    },
    checkedIn: (state, data) => {
        state.checkedIn = data;
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    actions
};
