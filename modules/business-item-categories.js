import axios from 'axios';

const getDefaultState = () => {
    return {
        itemCategories: {
            meta: {
                filter: {
                    range: {}
                }
            },
            data: [],
            links: {}
        },
    }
};

// initial state
const state = getDefaultState()

const actions = {
    list({ commit }, {slug, id = null}) {
        axios.get(`/api/${slug}/item-categories`, { params: { "category_id": id } }).then(response => {
            commit('storeCategories', response.data);
        });
    }

};

const mutations = {
    resetState(state) {
        Object.assign(state, getDefaultState())
    },
    storeCategories: (state, data) => {
        state.itemCategories = data;
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    actions
};
