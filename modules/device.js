import axios from "axios";

const state = {
  device: null,
  loading: false
};

const actions = {
  register({ commit }, params) {
    axios
      .post(`/api/devices/token`, {
        email: params.email,
        token: params.token,
        device: params.device
      })
      .then(response => {
        commit("saveDevice", response.data);
      });
  },
  loading({ commit }) {
    commit("loading", true);
  },
  loaded({ commit }) {
    setTimeout(() => {
      commit("loading", false);
    }, 100);
  }
};

const mutations = {
  saveDevice: (state, data) => {
    state.device = data;
  },
  loading: (state, load = true) => {
    state.loading = load;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
