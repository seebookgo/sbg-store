import moment from "moment";
import axios from "axios";

const getDefaultState = () => {
  return {
    service_id: null,
    business_slug: null,
    services: [],
    resources: [],
    times: [],
    date: null,
    time: null,
    dates: {
      availability: [],
      min: null,
      max: null
    },
    completed_booking: {
      business: {},
      service: {},
      resource: {}
    },
    profile_confirmed: false
  };
};

// initial state
const state = getDefaultState();

const actions = {
  services({ commit }, slug) {
    commit("setServices", { services: [] });
    return new Promise((resolve, reject) => {
      axios.get(`/api/${slug}/booking/services`).then(
        response => {
          commit("setServices", {
            services: response.data.data,
            business_slug: slug
          });
          resolve(response);
        },
        err => {
          reject(err);
        }
      );
    });
  },
  dates({ commit, state }, serviceId) {
    commit("setServiceAndDates", {
      service_id: null,
      dates: { availability: [] }
    });
    return new Promise((resolve, reject) => {
      axios.get(`/api/${state.business_slug}/booking/${serviceId}/dates`).then(
        response => {
          commit("setServiceAndDates", {
            service_id: serviceId,
            dates: {
              min: new Date(response.data.meta.start_at),
              max: new Date(response.data.meta.end_at),
              availability: response.data.data
            }
          });
          resolve(response.data);
        },
        err => {
          reject(err);
        }
      );
    });
  },
  times({ commit, state }, date) {
    commit("clearTimes");
    return new Promise((resolve, reject) => {
      axios
        .get(
          `/api/${state.business_slug}/booking/${state.service_id}/dates/${date}`
        )
        .then(
          response => {
            commit("setTimes", { date: date, times: response.data.data });
            resolve(response.data);
          },
          err => {
            reject(err);
          }
        );
    });
  },
  resources({ commit, state }, time) {
    commit("clearResources");
    return Promise.all([
      axios.get(
        `/api/${state.business_slug}/booking/${state.service_id}/dates/${state.date}/${time}`
      ),
      axios.get(`/api/${state.business_slug}/booking/${state.service_id}`)
    ])
      .then(response => {
        commit("setResources", {
          time: time,
          availability: response[0].data.data,
          resources: response[1].data.data
        });
      })
      .catch(error => {
        console.log(error);
      });
  },
  profileConfirmed({ commit }, val = true) {
    commit("profileConfirmed", val);
  },
  store(slug, booking) {
    return new Promise((resolve, reject) => {
      axios.post(`/api/${slug}/booking/`, booking).then(
        response => {
          resolve(response);
        },
        err => {
          reject(err);
        }
      );
    });
  },
  resetState({commit}) {
    commit('resetState');
  }
};

const mutations = {
  resetState(state) {
    Object.assign(state, getDefaultState());
  },
  setServices: (state, payload) => {
    state.services = [];
    payload.services.forEach(element => {
      state.services.push({
        value: element.id,
        text: element.service_description,
        duration: element.duration_format
      });
    });
    state.business_slug = payload.business_slug;
  },
  clearTimes: state => {
    state.date = null;
    state.times = [];
  },
  setTimes: (state, payload) => {
    state.date = payload.date;
    payload.times.forEach(element => {
      const time = moment.parseZone(element.time);
      state.times.push({
        value: time.format("HH:mm"),
        text: time.format("HH:mm"),
        disabled: !element.available
      });
    });
  },
  clearResources: state => {
    state.time = null;
    state.resources = [];
  },
  setResources: (state, payload) => {
    state.time = payload.time;
    state.resources = [];
    payload.resources.forEach(element => {
      if (payload.availability.includes(element.id)) {
        state.resources.push({
          value: element.id,
          text: element.name,
          description: element.description
        });
      }
    });
  },
  setServiceAndDates(state, payload) {
    state.service_id = payload.service_id;
    state.dates = payload.dates;
  },
  profileConfirmed(state, val) {
    state.profile_confirmed = val;
  },
  setCompletedBooking(state, payload) {
    state.completed_booking = payload;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
