const getDefaultState = () => {
    return {
        checkin: {
            showCheckinForm: false,
            showCheckinSuccess: false
        }
    }
};

// initial state
const state = getDefaultState()

const actions = {
    
};

const mutations = {
    resetState(state) {
        Object.assign(state, getDefaultState())
    },
    checkinForm: (state, data) => {
        state.checkin.showCheckinForm = data;
    },
    checkinSuccess: (state, data) => {
        state.checkin.showCheckinSuccess = data;
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    // actions
};
