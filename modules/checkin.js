import axios from "axios";

const getDefaultState = () => {
  return {
    checkins: {
      meta: {
        filter: {
          range: {}
        }
      },
      data: [],
      links: {
        first: null
      }
    },
    mode: "current",
    checkedIn: null,
    dateRange: {
      startDate: null,
      endDate: null
    },
    showCheckinForm: false
  };
};

// initial state
const state = getDefaultState();

const actions = {
  list({ commit, state }, page) {
    commit("clear");
    const params = {};

    if (state.mode == "past") {
      params["filter[past]"] = true;
    } else if (state.mode == "current") {
      params["filter[current]"] = true;
    } else if (state.mode == "cancelled") {
      params["filter[cancelled]"] = true;
    } else if (state.mode == "range") {
      params["filter[from]"] = state.dateRange.startDate;
      params["filter[to]"] = state.dateRange.endDate;
    }

    params["page"] = page;
    axios
      .get(`/api/check-ins`, {
        params
      })
      .then(response => {
        commit("store", response.data);
      });
  },
  status({ commit }, business) {
    axios.get(`/api/check-ins/status/${business}`).then(response => {
      if (response.data) {
        commit("checkedIn", response.data.data);
      }
    });
  },
  range({ commit, dispatch }, range) {
    commit("range", range);
    dispatch("list");
  },
  checkout({ dispatch }, checkin) {
    return new Promise((resolve, reject) => {
      axios
        .put(`/api/check-ins/${checkin.id}`, { action: "checkout" })
        .then(resp => {
          resolve(resp);
        })
        .catch(err => {
          dispatch("list");
          reject(err);
        });
    });
  },
  store(payload) {
    return new Promise((resolve, reject) => {
      axios
        .post("api/check-ins", payload)
        .then(resp => {
          resolve(resp);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
};

const mutations = {
  resetState(state) {
    Object.assign(state, getDefaultState());
  },
  clear: state => {
    state.checkins = {
      meta: {
        filter: {
          range: {}
        }
      },
      data: [],
      links: {}
    };
  },
  store: (state, data) => {
    state.checkins = data;
  },
  mode: (state, data) => {
    state.mode = data;
  },
  range: (state, payload) => {
    state.mode = "range";
    state.dateRange = {
      startDate: payload.startDate,
      endDate: payload.endDate
    };
  },
  checkedIn: (state, data) => {
    state.checkedIn = data;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
