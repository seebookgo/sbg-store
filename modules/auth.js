import axios from "axios";

const getDefaultState = () => {
  return {
    token: localStorage.getItem("user-token") || null,
    tutorial: localStorage.getItem("user-tutorial") || false,
    user: {profile: {}},
    populatedEmail: null,
    status: "default"
  };
};

// initial state
const state = getDefaultState();

const clientId = process.env.VUE_APP_CLIENT_ID ?? null;
const clientSecret = process.env.VUE_APP_CLIENT_SECRET ?? null;

const actions = {
  user({ commit }) {
    return new Promise((resolve, reject) => {
    axios.get(`/api/user`).then(response => {
      commit("saveUser", response.data.user);
      resolve(response.data.user);
    }, (err) => reject(err));
    });
  },
  login({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit("auth_request");
      axios
        .post("/oauth/token", {
          grant_type: "password",
          username: payload.username,
          password: payload.password,
          client_id: clientId,
          client_secret: clientSecret,
          scope: "*"
        })
        .then(resp => {
          axios.defaults.headers.common["Authorization"] =
            "Bearer " + resp.data.access_token;
          commit("login", resp.data.access_token);
          resolve(resp);
        })
        .catch(err => {
          commit("auth_error");
          localStorage.removeItem("user-token");
          reject(err);
        });
    });
  },
  register({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit("reg_request");
      axios
        .post("/register", payload)
        .then(resp => {
          if (resp.data.redirect == "app") {
            commit("reg_success", payload.email);
          } else {
            commit("reg_error");
          }
          resolve(resp);
        })
        .catch(err => {
          commit("reg_error");
          localStorage.removeItem("user-token");
          reject(err);
        });
    });
  },
  profile({ dispatch }, payload) {
    return new Promise((resolve, reject) => {
      axios
        .put("api/settings/contact", payload)
        .then(resp => {
          dispatch('user');
          resolve(resp);
        })
        .catch(err => {
          reject(err);
        });
    });
  },
  loadedTutorial({ commit }) {
    return new Promise(resolve => {
      commit("loadedTutorial");
      resolve(true);
    });
  },
  socialLogin({ commit }, provider) {
    return new Promise((resolve, reject) => {
      commit("auth_request", provider.name);
      axios
        .post("/oauth/token", {
          grant_type: "social",
          client_id: clientId,
          client_secret: clientSecret,
          provider: provider.name, // name of provider (e.g., 'facebook', 'google' etc.)
          access_token: provider.accessToken // access token issued by specified provider
        })
        .then(resp => {
          axios.defaults.headers.common["Authorization"] =
            "Bearer " + resp.data.access_token;
          commit("login", resp.data.access_token);
          resolve(resp);
        })
        .catch(err => {
          commit("auth_error");
          localStorage.removeItem("user-token");
          reject(err);
        });
    });
  },
  logout({ commit }) {
    return new Promise(resolve => {
      localStorage.removeItem("user-token");
      delete axios.defaults.headers.common["Authorization"];
      commit("browse/resetState", null, { root: true });
      commit("checkin/resetState", null, { root: true });
      commit("userBookings/resetState", null, { root: true });
      commit("resetState");
      commit("logout");
      resolve();
    });
  }
};

const mutations = {
  resetState(state) {
    Object.assign(state, getDefaultState());
  },
  auth_request(state, providerName) {
    if (providerName) {
      state.status = providerName + "Loading";
    } else {
      state.status = "loading";
    }
  },
  auth_error(state) {
    state.status = "login_error";
  },
  reg_request(state) {
    state.status = "loading";
  },
  reg_error(state) {
    state.status = "reg_error";
  },
  reg_success(state, email) {
    state.populatedEmail = email;
    state.status = "registered";
  },
  loadedTutorial(state) {
    state.tutorial = true;
    localStorage.setItem("user-tutorial", true);
  },
  login(state, token) {
    state.status = "success";
    localStorage.setItem("user-token", token);
    state.token = token;
  },
  logout(state) {
    localStorage.removeItem("user-token");
    state.token = null;
  },
  saveUser: (state, user) => {
    state.user = user;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
