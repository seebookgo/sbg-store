import axios from "axios";

const getDefaultState = () => {
  return {
    location: {},
    browseList: {
      meta: {},
      data: []
    },
    business: {
      data: {
        profile: {},
        services: []
      }
    }
  };
};

// initial state
const state = getDefaultState();

const actions = {
  getCategories({ commit }) {
    axios.get(`/api/browse/categories`).then(response => {
      this.loading = false;
      if (response.data) {
        commit("setCategories", response.data);
      }
    });
  },
  find({ commit }, { query, lat, lng, page = null }) {
    axios
      .get(`/api/browse?search=${query}&lat=${lat}&lng=${lng}&page=${page}`)
      .then(response => {
        this.loading = false;
        if (response.data) {
          commit("setBrowseList", response.data);
        }
      });
  },
  location({ commit }, search, lat, lng, page) {
    commit("setLocation", {});
    axios
      .get(`/api/browse?search=${search}&lat=${lat}&lng=${lng}&page=${page}`)
      .then(response => {
        this.loading = false;
        if (response.data) {
          commit("setLocation", response.data);
        }
      });
  },
  resetBusiness({ commit }) {
    commit("business", getDefaultState().business);
  },
  business({ commit, state }, slug) {
    if (!slug) return;

    if (state.business.slug != slug) {
      commit("business", getDefaultState().business);
    }
    return new Promise((resolve, reject) => {
      axios.get(`/api/${slug}/`).then(
        response => {
          if (response.data) commit("business", response.data);
          resolve();
        },
        err => {
          reject(err);
        }
      );
    });
  }
};

const mutations = {
  resetState(state) {
    Object.assign(state, getDefaultState());
  },
  setBrowseList: (state, data) => {
    state.browseList = data;
  },
  setLocation: (state, data) => {
    state.location = data;
  },
  business: (state, data) => {
    state.business = data;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
