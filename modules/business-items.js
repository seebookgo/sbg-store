import axios from "axios";

const getDefaultState = () => {
  return {
    items: {
      meta: {
        filter: {
          range: {}
        }
      },
      data: [],
      links: {}
    },
    item: {}
  };
};

// initial state
const state = getDefaultState();

const actions = {
  list({ commit }, { slug, id = null }) {
    axios
      .get(`/api/${slug}/items`, { params: { category_id: id } })
      .then(response => {
        commit("storeItems", response.data);
      });
  },
  show({ commit }, { slug, id }) {
    return new Promise(resolve => {
      axios.get(`/api/${slug}/items/${id}`).then(response => {
        commit("storeItem", response.data);
        resolve();
      });
    });
  }
};

const mutations = {
  resetState(state) {
    Object.assign(state, getDefaultState());
  },
  storeItems: (state, data) => {
    state.items = data;
  },
  storeItem: (state, data) => {
    state.item = data;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
