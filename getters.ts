const getters = {
  loading: (state: any) => state.device.loading,
  isAuthenticated: (state: any)=> state.auth.token != null,
  isBusiness: (state: any)=> (state.auth.user && state.auth.user.current_business != null),
  seenTutorial: (state: any)=> state.auth.tutorial,
  user: (state: any)=> state.auth.user,
  business: (state: any)=> state.auth.user.current_business ?? {},
  bookings: (state: any)=> state.userBookings.bookings,
  businesses: (state: any)=> state.browse.browseList,
  checkedIn: (state: any)=> state.checkin.checkedIn
};
export default getters;
//update