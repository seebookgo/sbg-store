import Vuex from "vuex";
import Vue from "vue";
import getters from "./getters";
import camelCase from "camelcase";

// https://webpack.js.org/guides/dependency-management/#requirecontext
const modulesFiles = require.context("./modules", false, /\.js$/);

interface StringArray { 
  [index: string]: any; 
}

const modules: StringArray = modulesFiles.keys().reduce((modules: StringArray, modulePath) => {
  const moduleName: string = camelCase(modulePath.replace(/^\.\/(.*)\.\w+$/, "$1"));
  const value = modulesFiles(modulePath);
  modules[moduleName] = value.default;
  return modules;
}, {});


Vue.use(Vuex);


const store = new Vuex.Store({
  modules,
  getters
});
export default store;
